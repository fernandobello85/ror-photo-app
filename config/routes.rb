Rails.application.routes.draw do
  root 'welcome#index'
  resources :images
  resources :users, only: [:show, :destroy], :constraints => { :id => /[0-9|]+/ }
  devise_for :users, :controllers => { :registrations => 'registrations' }
end
