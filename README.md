# README

This is a simple Ruby on Rails web application proposed by the Complete Ruby on Rails Developer course. 
To improve the use of email confirmation and payment when signup with premium account.
Using Sendgrid and Stripe. 


Things you may want to cover:

* Ruby version 2.6.3
* Rails version 6.0.3.2
* Bootstrap 4.3.1
* Stripe 4.24.0 
* Carrierwave 2.1.0


